export { default as MainContainer } from '../..\\components\\MainContainer.vue'
export { default as CommonActionContainer } from '../..\\components\\common\\ActionContainer.vue'
export { default as CommonButton } from '../..\\components\\common\\Button.vue'
export { default as CommonPostContainer } from '../..\\components\\common\\PostContainer.vue'
export { default as PostListsListOfActions } from '../..\\components\\PostLists\\ListOfActions.vue'
export { default as PostListsSortablePostList } from '../..\\components\\PostLists\\SortablePostList.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}

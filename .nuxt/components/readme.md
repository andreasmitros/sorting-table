# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<MainContainer>` | `<main-container>` (components/MainContainer.vue)
- `<CommonActionContainer>` | `<common-action-container>` (components/common/ActionContainer.vue)
- `<CommonButton>` | `<common-button>` (components/common/Button.vue)
- `<CommonPostContainer>` | `<common-post-container>` (components/common/PostContainer.vue)
- `<PostListsListOfActions>` | `<post-lists-list-of-actions>` (components/PostLists/ListOfActions.vue)
- `<PostListsSortablePostList>` | `<post-lists-sortable-post-list>` (components/PostLists/SortablePostList.vue)

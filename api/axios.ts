import axios, { AxiosError, AxiosResponse } from 'axios'

const customAxios = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com'
})
customAxios.interceptors.response.use(
    async (response:AxiosResponse) =>  response.data,
    (error:AxiosError) => {
        console.error('An error occured with status:',error?.response?.status)
        return []
    }
)

export const client = async (url:string) : Promise<any>=> {
    return await customAxios.get(url)
}
import { client } from "./axios";
import { Post } from "~/types";
export const getPosts = async () : Promise<Array<Post>>=> await client('/posts')
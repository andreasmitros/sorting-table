// store/index.js

import { NuxtState } from "@nuxt/types/app"
import { ListActions, CustomContext, FilteredPosts } from "~/types";


export const state = () => ({
    posts:[] as Array<FilteredPosts>,
    listActions: [] as Array<ListActions>
})

export const mutations = {
    addPosts(state:NuxtState, posts:Array<FilteredPosts>){
        state.posts = posts
    },
    addListActions(state:NuxtState, listAction:ListActions){
        //clone listActions from state to process
        const listActions : Array<ListActions> = JSON.parse(JSON.stringify(state.listActions))
        // shift new item from the beggining of the array
        listActions.unshift(listAction)
        // save index to map time travel action
        listActions.forEach((action:ListActions, index:number) => action.index = index)
        state.listActions = listActions
    },
    addTimeTravel(state:NuxtState, listActions:Array<ListActions>){
        state.listActions = listActions
    }
}

export const actions = {
    setPosts({ commit }:CustomContext , posts:Array<FilteredPosts>){
        commit('addPosts', posts)
    },
    setListActions({commit}: CustomContext, listAction:ListActions) {
        commit('addListActions', listAction)
    },
    setTimeTravel({commit}: CustomContext, listActions:Array<ListActions>){
        commit('addTimeTravel', listActions)
    }
}



export const getters = {
    getPosts(state:NuxtState){
        return state.posts
    },
    getListActions(state:NuxtState){
        return state.listActions
    }
}
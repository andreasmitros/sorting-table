const posts = [
    {
        "userId": 1,
        "id": 1,
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        "userId": 1,
        "id": 2,
        "title": "qui est esse",
        "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    },
    {
        "userId": 1,
        "id": 3,
        "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        "body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
    },
    {
        "userId": 1,
        "id": 4,
        "title": "eum et est occaecati",
        "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
    },
    {
        "userId": 1,
        "id": 5,
        "title": "nesciunt quas odio",
        "body": "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
    },
    {
        "userId": 1,
        "id": 6,
        "title": "dolorem eum magni eos aperiam quia",
        "body": "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae"
    },
]

const post = {
    "userId": 1,
    "id": 6,
    "title": "dolorem eum magni eos aperiam quia",
    "body": "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae"
}

const ListOfActionsProps = {
    index:1,
    nextIndex: 1,
    postId:2,
    postIdsOrder: [3,4,6,2,1],
    prevIndex:3
}

import { mount } from "@vue/test-utils"
import Button from '@/components/common/Button'
import PostContainer from '@/components/common/PostContainer'
import SortablePostList from '@/components/PostLists/SortablePostList'

describe('Mount components', () => {
  test('mount button', () => {
    const wrapper = mount(Button)
    expect(wrapper.vm).toBeTruthy()
  })

  test('mount Post container', () => {
    const wrapper = mount(PostContainer, {
        propsData:{
            post,
            showUp : true,
            showDown : false
        }
    })
    expect(wrapper.vm).toBeTruthy()
    expect(wrapper.props().showUp).toBe(true),
    expect(wrapper.props().showDown).toBe(false)
    expect(wrapper.props().post).toBe(post)

  })

  test('test props on SortablePostList component props', () => {
    const ListOfActionsWrapper = mount(SortablePostList, {
        propsData:{
            posts
        }
    })
    expect(ListOfActionsWrapper.vm).toBeTruthy()
    expect(ListOfActionsWrapper.props().posts).toBe(posts)
  })

  
  test('test mounting SortablePostList component with empty Posts array', () => {
    const emptyPosts = []
    const ListOfActionsWrapper = mount(SortablePostList, {
        propsData:{
           posts: emptyPosts
        }
    })
    expect(ListOfActionsWrapper.vm).toBeTruthy()
  })
})
